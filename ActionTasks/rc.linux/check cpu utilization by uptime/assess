import java.text.DecimalFormat;

def condition    = "good";
def severity     = "";
def summary      = "";
def detail       = "";

// Input parameters
def command           = INPUTS.CMD_SENT?:"";
def lastXMin          = INPUTS.LAST_X_MIN?:"";
def workLog           = INPUTS.WORKLOG?: ""; 
def cmdOutput         = INPUTS.CMD_OUTPUT?:"";
def numOfCPU          = INPUTS.NUM_OF_CPU?:"";
def severeThreshold   = INPUTS.SEVERE_CPU_THRESHOLD?:"";
def criticalThreshold = INPUTS.CRITICAL_CPU_THRESHOLD?:"";
def verbose           = INPUTS.VERBOSE.toString().toUpperCase() == "TRUE";

// Variables to store outputs.
def errorMessage = "";
def mainContent = "";

try
{	
	if(INPUTS.RSEXCEPTION) 
	{
        summary = INPUTS.RSEXCEPTION_SUMMARY;
        detail = INPUTS.RSEXCEPTION_DETAIL;
        errorMessage = summary ?: "Unknown error";  
	}
	else if(!cmdOutput)
	{
		errorMessage = "Command [ ${INPUTS.CMD_SENT} ] - No command outout is available.";
	}
	else if(!numOfCPU)
	{
		errorMessage = "Failed to compute number of CPU";
	}
	else if(!DATA)
	{
		errorMessage = "Failed to parse command output: " + cmdOutput.trim();
	}
	else
	{
		def lines = cmdOutput.split('\n');
		def size = lines.length;
		for(int i=1; i<size-1; i++)
		{
			mainContent += "\n" + lines[i];
		}
		mainContent = mainContent.trim();	
		
		if(cmdOutput.contains("unknown command") || cmdOutput.contains("Invalid command") || cmdOutput.contains("command not found") || cmdOutput.contains("command not found"))
		{
			severity = "warning";
			errorMessage = "Command [ ${INPUTS.CMD_SENT} ] not recognized."
		}
		else if(cmdOutput.contains("Permission denied"))
		{
		    severity = "warning";
		}
		else
		{
			def loadAverage = "";
			
			if(lastXMin=="1")
			{
				loadAverage = DATA.MIN1_LOAD?.toDouble();
			}
			else if(lastXMin=="5")
			{
				loadAverage = DATA.MIN5_LOAD?.toDouble();
			}
			else if(lastXMin=="15")
			{
				loadAverage = DATA.MIN15_LOAD?.toDouble();
			}
			
			def utilizationPercentagePerCore;
			if(loadAverage)
			{
				utilizationPercentagePerCore = (loadAverage*100)/numOfCPU;
			}
			else if(loadAverage==0)
			{
				utilizationPercentagePerCore = 0;
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
			dbPoolUsageRatio =	df.format(utilizationPercentagePerCore);
			utilizationPercentagePerCore = Double.parseDouble(dbPoolUsageRatio);			
			
			// Default main content
		    mainContent += "\nCPU utilization percentage (${lastXMin} minutes average) per core is ${utilizationPercentagePerCore}%";
			
			// Grep threshold values.
	        severeThreshold = severeThreshold.toDouble();
	        criticalThreshold = criticalThreshold.toDouble();

			if (utilizationPercentagePerCore > criticalThreshold)
			{
				severity= "severe";
				mainContent += "\nCPU utilization percentage: ${utilizationPercentagePerCore}% is higher than critical threshold value: ${criticalThreshold}%";  
			}
			else if (utilizationPercentagePerCore > severeThreshold) 
			{
 				severity="warning";
				mainContent += "\nCPU utilization percentage: ${utilizationPercentagePerCore}% is higher than severe threshold value: ${severeThreshold}%";             
			}			
			else
			{
				// CPU utilization is healthy.
				severity = "good";
	        }	
	        mainContent = "Number of CPU: " + numOfCPU + "\n" + mainContent
		}		

		workLog += cmdOutput ? "\n${cmdOutput}\n" : "\nNo output received from command\n";
	}		
	workLog += "=" * 80;
	workLog = "\n" + workLog;
}
catch (Exception e)
{
    errorMessage   = "Unexpected Exception: ${e.getClass()}: ${e.getMessage()}";
    detail    = "Unexpected Exception: ${e.getClass()}: ${e.getMessage()}";
    e.getStackTrace().each() 
    { 
    	detail += "\n\t${it}"; 
    }
}

if(errorMessage)
{
    condition = "bad";
    severity  = severity?:"critical";
    summary = summary?: errorMessage;
    detail = detail?: errorMessage;
}
else
{
   summary = "Compute CPU utilization percentage by (load average over x min)*100/number of CPUs.";
   summary += "\n--------------------------------------------------------------------------------";
   summary += "\n" + mainContent;	
}

workLog += "\n----------${command}----------\n" + summary.trim();


OUTPUTS.ERROR_MESSAGE = errorMessage;
OUTPUTS.CMD_OUTPUT    = cmdOutput;
OUTPUTS.MAIN_CONTENT  = mainContent;
OUTPUTS.WORKLOG      = workLog;

RESULT.condition = condition;
RESULT.severity  = severity;
RESULT.summary   = summary;
RESULT.detail    = detail;